module bitbucket.org/sothy5/n4-go

go 1.12

require (
	github.com/fiorix/go-diameter v3.0.2+incompatible
	github.com/sirupsen/logrus v1.5.0 // indirect
	github.com/u-root/u-root v6.0.0+incompatible
	github.com/urfave/cli v1.22.3 // indirect
)
