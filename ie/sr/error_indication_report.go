package sr

import (
	"bitbucket.org/sothy5/n4-go/ie"
)

// ApplicationDetectionInformation
type ErrorIndicationReport struct {
	RemoteFTEID *ie.InformationElement
}
